public class HashMap<K,T> {

    private int size = 1;
    private Object[] keys;
    private Object[] values;

    public HashMap(){
        keys = new Object[size];
        values = new Object[size];
    }

    void put(K key, T value){
        keys[size-1] = key;
        values[size-1] = value;
        size++;
        updateList();
    }

    T getValue(K key){
        int index = getIndex(key);
        return (T)values[index];
    }

    void clear(){
        size = 1;
        keys = new Object[size];
        values = new Object[size];
    }

    boolean containsKey(K key){
        if(getIndex(key)>=0){
            return true;
        }else{
            return false;
        }
    }

    int length(){
        return size-1;
    }

    private int getIndex(K key){

        int pos = -1;
        for (int i = 0;i < size-1;i++){

            if(keys[i].equals(key)){
                pos = i;
            }

        }
        return pos;
    }

    private void updateList(){

        Object tempK[] = new Object[size];
        Object tempV[] = new Object[size];
        for(int i =0;i < size-1;i++){
            tempK[i] = keys[i];
            tempV[i] = values[i];
        }
        keys = tempK;
        values = tempV;
    }

    //Test my Hashmap that I have created
    public static void main(String args[]){

        HashMap<Integer,String> hashMap = new HashMap<Integer,String>();
        hashMap.put(1,"Isik");
        hashMap.put(2,"Turkey");
        hashMap.put(3,"Jamaica");
        if(hashMap.containsKey(3)){
            System.out.println("3 = " +  hashMap.getValue(3));
        }
        System.out.println("size: " + hashMap.length());
        hashMap.clear();
    }
}
